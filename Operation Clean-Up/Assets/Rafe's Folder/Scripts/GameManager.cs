using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] int currentTrash;
    [SerializeField] int totalTrash;

    [SerializeField] float time;
    [SerializeField] TextMeshProUGUI timerText;

    public static GameManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;

            int minutes = Mathf.FloorToInt(time / 60);
            int seconds = Mathf.FloorToInt(time % 60);

            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

            if (time <= 0)
                Lose();
        }

    }

    public void addCurrentTrash()
    {
        currentTrash += 1;

        if (currentTrash == totalTrash)
        {
            Win();
        }
    }

    private void Lose()
    {
        timerText.text = "00:00";

        // Pause
        Time.timeScale = 0;

        // Show UI
    }
    
    private void Win()
    {
        // Pause
        Time.timeScale = 0;

        // Show UI
    }

}

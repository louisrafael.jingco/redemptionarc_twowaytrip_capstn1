using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Mop : MonoBehaviour
{
    int level;
    public Transform attackPoint;
    public float attackRadius;
    public float upgradeAmount;

    // Start is called before the first frame update
    void Start()
    {
        level = 1;
    }

    public void MopAttack(InputAction.CallbackContext context)
    {
        Debug.Log("Mop Attack!");

        // Play Attack Animation

        // Detect Enemies in Range of Attack
        Collider2D[] hitStains = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius);

        // Damage them
        foreach (Collider2D stain in hitStains)
        {
            if (stain.CompareTag("Stains"))
            {
                Debug.Log("Stain Cleaned!");
                Destroy(stain.gameObject);
            }
        }
    }

    public void MopUpgrade()
    {
        attackRadius += upgradeAmount;
    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }
}

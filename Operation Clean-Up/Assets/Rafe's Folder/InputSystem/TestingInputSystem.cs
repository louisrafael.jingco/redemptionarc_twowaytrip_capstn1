using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestingInputSystem : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody2D rigidBody;

    float moveSpeed;
    [SerializeField] float walkSpeed;
    [SerializeField] float runSpeed;

    [SerializeField] float stamina;
    float maxStamina;
    bool isResting;

    Mop mop;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        moveSpeed = walkSpeed;
        maxStamina = stamina;
        isResting = false;

        playerInputActions = new PlayerInputActions();
        playerInputActions.Player.Enable();

        playerInputActions.Player.Run.performed += Run;
        playerInputActions.Player.Run.canceled += Run;

        mop = GetComponent<Mop>();
        playerInputActions.Player.Mop.performed += mop.MopAttack;
    }

    private void Update()
    {
        staminaBar();
    }

    private void Run(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            moveSpeed = walkSpeed;
            return;
        }

        if (isResting == false)
            moveSpeed = runSpeed;
    }
    private void staminaBar()
    {
        if (playerInputActions.Player.Run.IsPressed() && isResting == false)
        {
            stamina -= Time.deltaTime;

            if (moveSpeed == walkSpeed)
                moveSpeed = runSpeed;
        }


        if (!playerInputActions.Player.Run.IsPressed() || isResting == true)
        {
            if (stamina < maxStamina)
                stamina += Time.deltaTime;

            if (stamina >= maxStamina && isResting == true)
                isResting = false;
        }

        if (stamina <= 0)
        {
            isResting = true;
            moveSpeed = walkSpeed;
        }
    }

    private void FixedUpdate()
    {
        Vector2 inputVector = playerInputActions.Player.Walk.ReadValue<Vector2>();

        if (inputVector.x == 0 && inputVector.y == 1 || inputVector.x == 0 && inputVector.y == -1 || inputVector.x == -1 && inputVector.y == 0 || inputVector.x == 1 && inputVector.y == 0)
        {
            float angle = Vector2ToAngleInDegrees(inputVector);
            rigidBody.SetRotation(angle);

            rigidBody.MovePosition((Vector2)transform.position + (inputVector * moveSpeed * Time.deltaTime));
        }

    }

    float Vector2ToAngleInDegrees(Vector2 vector)
    {
        float angleInRadians = Mathf.Atan2(vector.y, vector.x);
        float angleInDegrees = angleInRadians * Mathf.Rad2Deg;
        return angleInDegrees;
    }
}
